import allure
from pages.Old_Pages.DailyAESummryReportPage import DailyAESummaryReportPage
from pages.HomePage import HomePage
import time


@allure.epic("Home")
class Test3:                                          # наименование класса должно начинаться с: Test

    @allure.title("Log in with a valid user")         # наименование отчета о тесте
    @allure.description("Log in with a valid user")   # писание отчета о тесте
    def test_3(self, localization, selenium_facade):  # наименование метода теста должно начинаться с: test_

        locator = DailyAESummaryReportPage()          # создаем объекты сраниц
        home_page = HomePage()


        with allure.step("Step 1. Click 3 menus on Home page"):     # прописываем шаги тестов
            selenium_facade.click_button(home_page.get_menu_item_data_sets())
            # selenium_facade.element_wait_to_be_visible(home_page.get_menu_item_daily_provider_data_sets())
            selenium_facade.move_to_element(home_page.get_menu_item_daily_provider_data_sets())
            selenium_facade.click_button(home_page.get_menu_item_daily_a_e_summary_report())


        with allure.step("Step 2. Examine the page title"):
            selenium_facade.element_wait_to_be_visible(locator.get_iframe_main())  # ждем пока загрузится страница и будет доступен элемент нужного iframe
            selenium_facade.element_wait_frame_to_be_available_and_switch_to_it(locator.get_iframe_main())  # переключаемся в нужный iframe
            selenium_facade.explicit_wait(8)   # ожидаем пока полностью подгрузится страница и будет доступен элемент тайтла
            selenium_facade.element_wait_to_be_visible(locator.get_title_daily_a_e_summary_report())
            assert selenium_facade.get_text(locator.get_title_daily_a_e_summary_report()) == localization.get_value("titles", "daily_ae_summary_report")

        with allure.step("Step 3. Examine the Age dimension"):
            selenium_facade.click_button(locator.get_dimension_check_box())

            assert len(selenium_facade.get_elements_count(locator.get_dimension_count())) == 15 # проверяем количество дименшинов в выпадающем сиске

            selenium_facade.click_button(locator.get_item_age())
            selenium_facade.click_button(locator.get_button_update())

        with allure.step("Step 4. Examine metrics"):
            selenium_facade.click_button(locator.get_metrics_menu())
            selenium_facade.explicit_wait(2)
            selenium_facade.switch_to_default()  # возвращаемся к дефолтному фрейму
            selenium_facade.element_wait_frame_to_be_available_and_switch_to_it(locator.get_iframe_metrics())   # переключаемся в нужный iframe

            assert selenium_facade.get_text(locator.get_title_select_metrics()) == \
                   localization.get_value("titles", "select_metrics")

            selenium_facade.explicit_wait(5)

            # selenium_facade.element_wait(locator.get_metric_No_of_Attendances())
            selenium_facade.double_click(locator.get_metric_No_of_Attendances())
            selenium_facade.double_click(locator.get_metric_No_of_Patients())
            selenium_facade.click_button(locator.get_metric_button_Apply())
            # selenium_facade.context_click(locator.get_metric_No_of_Attendances())

            selenium_facade.switch_to_default()
            selenium_facade.element_wait_frame_to_be_available_and_switch_to_it(locator.get_iframe_main())
            selenium_facade.click_button(locator.get_button_update())
            selenium_facade.explicit_wait(5)
            selenium_facade.element_wait_to_be_visible(locator.get_metric_No_of_Attendances_value_1())
            selenium_facade.click_button(locator.get_metric_No_of_Attendances_value_1()) # кликнуть на значение метрики 1371

        with allure.step("Step 5. Detail report"):
            selenium_facade.switch_to_default()
            selenium_facade.element_wait_frame_to_be_available_and_switch_to_it(locator.get_iframe_main())

            # selenium_facade.explicit_wait(5)
            assert selenium_facade.get_text(locator.get_title_Daily_A_E_Detail_Report()) == \
                localization.get_value("titles", "Daily_A_E_Detail_Report")

            time.sleep(2)




