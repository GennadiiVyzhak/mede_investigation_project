import allure
from pages.Old_Pages.DailyAESummryReportPage import DailyAESummaryReportPage
from pages.HomePage import HomePage


@allure.epic("Home")
class TestLoginGettingError:

    @allure.title("Log in with a nonexistent user and get a validation message")
    @allure.description("Log in with a nonexistent user and get a validation message")
    def test_log_in_and_get_error(self, localization, selenium_facade):

        daily_ae_summary_report_page = DailyAESummaryReportPage()
        home_page = HomePage()

        with allure.step("Step 1. Click 3 menus on Home page"):
            selenium_facade.click_button_if_visible(home_page.get_menu_item_data_sets())
            selenium_facade.element_wait_to_be_visible(home_page.get_menu_item_daily_provider_data_sets())
            selenium_facade.move_to_element(home_page.get_menu_item_daily_provider_data_sets())
            selenium_facade.click_button_if_visible(home_page.get_menu_item_daily_a_e_summary_report())

        # with allure.step("Step 2. Examine the page title"):
        #     selenium_facade.element_wait(daily_ae_summary_report_page.get_title_daily_a_e_summary_report())
        #     assert selenium_facade.get_text(daily_ae_summary_report_page.get_title_daily_a_e_summary_report()) == \
        #            localization.get_value("titles", "daily_ae_summary_report")
        #     time.sleep(100)

        with allure.step("Step 2. Examine the dimensions"):
            selenium_facade.click_button_if_visible(daily_ae_summary_report_page.get_dimension_check_box())
            selenium_facade.click_button_if_visible(daily_ae_summary_report_page.get_dimension_field())


