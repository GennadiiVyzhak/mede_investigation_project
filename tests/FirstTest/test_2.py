import allure
from pages.Old_Pages.DailyAESummryReportPage import DailyAESummaryReportPage
from pages.HomePage import HomePage
import time


@allure.epic("Home")
class TestLoginGettingError:

    @allure.title("Log in with a nonexistent user and get a validation message")
    @allure.description("Log in with a nonexistent user and get a validation message")
    def test_log_in_and_get_error(self, localization, selenium_facade):

        # daily_ae_summary_report_page = DailyAESummaryReportPage()
        # home_page = HomePage()

        ae_summary_page = DailyAESummaryReportPage()
        home_page = HomePage()


        with allure.step("Step 1. Click 3 menus on Home page"):
            selenium_facade.click_button_if_visible(home_page.get_menu_item_data_sets())
            # selenium_facade.element_wait_to_be_visible(home_page.get_menu_item_daily_provider_data_sets())
            selenium_facade.move_to_element(home_page.get_menu_item_daily_provider_data_sets())
            selenium_facade.click_button_if_visible(home_page.get_menu_item_daily_a_e_summary_report())

        with allure.step("Step 2. Examine the page title"):
            selenium_facade.element_wait_to_be_visible(ae_summary_page.get_iframe_main())
            selenium_facade.element_wait_frame_to_be_available_and_switch_to_it(ae_summary_page.get_iframe_main())
            selenium_facade.explicit_wait(8)
            selenium_facade.element_wait(ae_summary_page.get_title_daily_a_e_summary_report())
            assert selenium_facade.get_text(ae_summary_page.get_title_daily_a_e_summary_report()) == \
                   localization.get_value("titles", "daily_ae_summary_report")

        with allure.step("Step 3. Examine the Age dimension"):
            selenium_facade.click_button(ae_summary_page.get_dimension_check_box())
            selenium_facade.click_button(ae_summary_page.get_item_age())
            selenium_facade.click_button_if_visible(ae_summary_page.get_button_update())

        with allure.step("Step 3. Examine metrics"):
            selenium_facade.click_button(ae_summary_page.get_metrics_menu())
            # selenium_facade.explicit_wait(5)
            selenium_facade.switch_to_default()

            selenium_facade.element_wait_to_be_visible(ae_summary_page.get_iframe_metrics())
            selenium_facade.element_wait_frame_to_be_available_and_switch_to_it(ae_summary_page.get_iframe_metrics())

            selenium_facade.element_wait(ae_summary_page.get_metric_No_of_Attendances())
            selenium_facade.double_click(ae_summary_page.get_metric_No_of_Attendances())
            selenium_facade.context_click(ae_summary_page.get_metric_No_of_Patients())
            # selenium_facade.click_button(ae_summary_page.get_metric_No_of_Attendances())

            # selenium_facade.click_button(ae_summary_page.get_item_age())
            time.sleep(30)




