# The guide for running ui-tests

- Install Python 3.7 https://www.python.org/downloads/ (use the latest stable version)

- Install Java (use java 8)

- Install virtualenv package in the project

$ pip install virtualenv

- Create a virtual environment with, for example, "venv" identifier

$ virtualenv venv

- Activate the virtual environment

$ source venv/bin/activate (for Mac)

$ venv\Scripts\activate (for PC)

- Install Allure via Homebrew 

$ brew install allure

For Windows, Allure is available from the Scoop commandline-installer.

- To install Allure, download and install Scoop and then execute in the Powershell (write "PowerShell" and open it).

Execute the command in Powershell:

$ Set-ExecutionPolicy RemoteSigned -scope CurrentUser

Then execute the following command in Powershell:

$ iex (new-object net.webclient).downloadstring('https://get.scoop.sh')

Use Powershell again and execute the following command:

$ scoop install allure

Also Scoop is capable of updating Allure distribution installations. To do so navigate to the Scoop installation directory and execute

$ \bin\checkver.ps1 allure -u

This will check for newer versions of Allure, and update the manifest file. Then execute

$ scoop update allure

to install a newer version.

- Go to "Project Name" folder and install the packages which are necessary to have the tests work

$ cd <Project Name>

$ pip install -r requirements.txt

- allure for pytest can be installed (if it is not in requirements.txt) in the following way:

Use a command line and execute

$ pip install allure-pytest

- Download Selenium Server Standalone https://www.seleniumhq.org/download/, 
for example selenium-server-standalone-3.141.59.jar, and put it in the project's root

- Download ChromeDriver https://sites.google.com/a/chromium.org/chromedriver/downloads
- Download Geckodriver for Firefox https://github.com/mozilla/geckodriver/releases
- Download IEDriverServer for IE11 https://www.seleniumhq.org/download/ ('The Internet Explorer Driver Server' title)
All of the downloaded files put in the project's root

- Before we run the tests we need to run Selenium Standalone Server as a hub in the first console window
 (do it from the project's root)
 OR just put a webdriver into the project's folder and run tests without Selenium Standalone Server

$ java -jar selenium-server-standalone-<version>.jar -role hub

- After that we need to create nodes where we'll run our tests (do it in the second console window)

$ java -jar selenium-server-standalone-<version>.jar -role node -hub http://localhost:4444/grid/register -port 5555

- Before running the test we need to go to conftest.py file and change IP address to the address of your machine
You can find it executing the next command in console:

$ ipconfig

Get IPv4 Address value from there and put it in 'driver' fixture in conftest.py instead of the written there 

How to run tests:
- STAGE: run all tests with creating allure reports

For Chrome:
$ pytest -q -v --config="stage" --browser="chrome" --alluredir ./allure-reports

For Firefox:
$ pytest -q -v --config="stage" --browser="firefox" --alluredir ./allure-reports

For IE11:
$ pytest -q -v --config="stage" --browser="ie" --alluredir ./allure-reports

For Edge:
$ pytest -q -v --config="stage" --browser="edge" --alluredir ./allure-reports

- UAT: run all tests on UAT environment with creating allure reports

For Chrome:
$ pytest -q -v --config="uat" --browser="chrome" --alluredir ./allure-reports

For Firefox:
$ pytest -q -v --config="uat" --browser="firefox" --alluredir ./allure-reports

For IE11:
$ pytest -q -v --config="uat" --browser="ie" --alluredir ./allure-reports

For Edge:
$ pytest -q -v --config="uat" --browser="edge" --alluredir ./allure-reports

- For both STAGE and UAT Generate report as HTML file

$ allure serve ./allure-reports