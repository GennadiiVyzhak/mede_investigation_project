
class DailyAEDetailReport:

    """  Titles locators: """
    # _title_Daily_A_E_Detail_Report = "//a[contains(text(),'Daily A&E Detail Report')]"
    _title_Daily_A_E_Detail_Report = "//h1[@class='detail-title'][contains(text(),'Daily A&E Detail Report')]"

    """  Field locators:  """
    _Age_field = "//th[@class='green']//span[contains(text(),'Age')]"
    _Age_field_checkbox = "//table[@id='tblMain']//th[4]//div[1]//span[3]"

    """  Button locators:  """
    _Group_button = "//ul[@class='dropdown-menu']//a[contains(text(),'Group')]"

    """  Frame locators:  """
#   _iframe_field = '//*[@id="GroupByViewContainer"]'
#   _iframe_field = "/html/body/div[1]/div[5]/div[2]/div[2]/iframe"
    _iframe_field = "//iframe[contains(@id, 'dialogFrame')]"
    _iframe_field_Update_button = "//button[contains(text(),'Update')]"

    """  Assert locators:  """
    _field_Age_Query_totals_value = "//tr[@id='queryRow']//span[1]//span[1]"




    """  Titles methods: """
    def get_title_Daily_A_E_Detail_Report(self):
        return self._title_Daily_A_E_Detail_Report

    """  Field methods:  """
    def get_Age_field(self):
        return self._Age_field

    def get_Age_field_checkbox(self):
        return self._Age_field_checkbox

    """  Button methods:  """
    def get_Group_button(self):
        return self._Group_button

    """  Frame methods:  """
    def get_iframe_field(self):
        return self._iframe_field

    def get_iframe_field_Update_button(self):
        return self._iframe_field_Update_button

    """  Assert methods:  """
    def get_field_Age_Query_totals_value(self):
        return self._field_Age_Query_totals_value

