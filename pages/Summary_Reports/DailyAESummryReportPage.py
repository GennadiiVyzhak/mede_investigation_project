
class DailyAESummaryReport:

# Titles locators:
    _title_daily_a_e_summary_report = "//h3[contains(text(),'Daily A&E Summary Report')]"
    _title_select_metrics = "//span[contains(text(),'Select Metrics')]"

# Button locators:
    _button_Update = "//div[@id='container-dimpanel']//div//a[contains(text(),'Update')]"

# Dimension locators:
    _dimension_check_box = "//div[@id='s2id_autogen1']//b"
    # _dimension_check_box = "//div[@id='s2id_autogen1']//a//span//b"
    _dimension_field = "//input[@id='s2id_autogen2_search']"
    _dimension_Age = "//div[text()='Age']"
    _dimension_Age_value_1 = "//tr[@id='tr_TC0_0_0']//td[2]//span[1]"


# Metric frame locators:
    _iframe_metrics = "//iframe[@id='dialogFrame0']"
    _iframe_metric_menu = "//a[@class='btn']"
    _iframe_metric_button_Apply = "//span[contains(text(),'Apply')]"
    _iframe_metric_No_of_Attendances = "//a[contains(text(),'% No. of Attendances')]"
    _iframe_metric_No_of_Patients = "//a[contains(text(),'% No. of Patients')]"

# Metric locators:
    _metric_No_of_Patients = "//body//div[@id='reportTableContainer_TC0']//div//div[1]//table[1]//thead[1]//tr[1]//td[3]//a[1]"
    _metric_No_of_Attendances = "//body//div[@id='reportTableContainer_TC0']//div//div[1]//table[1]//thead[1]//tr[1]//td[2]//a[1]"

# Metric value locators:
    _metric_No_of_Patients_value_1 = "//tr[@id='tr_TC0_0_0']//td[3]//span[1]"  # 1014
#   _metric_No_of_Attendances_value_1 = "//tr[@id='tr_TC0_0_0']//td[2]//span[1]"  # 1371
    _metric_No_of_Attendances_value_1 = "/html[1]/body[1]/div[1]/div[1]/div[1]/div[4]/div[2]/div[2]/div[1]/div[2]/div[2]/table[1]/tbody[1]/tr[1]/td[2]/span[1]"

# Common locators:
    _spinner = "span.icon.icon-circle-o-notch.icon-xx-large.icon-spin::before"
    _dimension_count = "//*[@class='select2-result-label']"



    """  Titles methods: """
    def get_title_daily_a_e_summary_report(self):
        return self._title_daily_a_e_summary_report

    def get_title_select_metrics(self):
        return self._title_select_metrics

    """  Button methods:  """
    def get_button_Update(self):
        return self._button_Update

    """  Dimension methods:  """
    def get_dimension_check_box(self):
        return self._dimension_check_box

    def get_dimension_field(self):
        return self._dimension_field

    def get_dimension_Age(self):
        return self._dimension_Age

    def get_dimension_Age_value_1(self):
        return self._dimension_Age_value_1

    """  Metric frame methods:  """
    def get_iframe_metrics(self):
        return self._iframe_metrics

    def get_iframe_metric_menu(self):
        return self._iframe_metric_menu

    def get_iframe_metric_button_Apply(self):
        return self._iframe_metric_button_Apply

    def get_iframe_metric_No_of_Attendances(self):
        return self._iframe_metric_No_of_Attendances

    def get_iframe_metric_No_of_Patients(self):
        return self._iframe_metric_No_of_Patients

    """  Metric methods:  """
    def get_metric_No_of_Patients(self):
        return self._metric_No_of_Patients

    def get_metric_No_of_Attendances(self):
        return self._metric_No_of_Attendances

    """  Metric value methods:  """

    def get_metric_No_of_Patients_value_1(self):
        return self._metric_No_of_Patients_value_1

    def get_metric_No_of_Attendances_value_1(self):
        return self._metric_No_of_Attendances_value_1

    """  Common methods:  """

    def get_spinner(self):
        return self._spinner

    def get_dimension_count(self):
        return self._dimension_count

