
class LoginPage:

    _btn_login = "#ctl00_mainContentPlaceHolder_LogInButton"
    _input_password = "#ctl00_mainContentPlaceHolder_passwordControl"
    _input_username = "//input[@id='ctl00_mainContentPlaceHolder_loginControl']"

    def get_btn_login(self):
        return self._btn_login

    def get_input_password(self):
        return self._input_password

    def get_input_username(self):
        return self._input_username
