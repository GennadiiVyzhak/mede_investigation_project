
class CommonPage:


# General Frame locators:
    _iframe_main = "#frmMain"
    _iframe_metrics = "//iframe[@id='dialogFrame0']"



# General Frame methods:
    def get_iframe_main(self):
        return self._iframe_main

    def get_iframe_metrics(self):
        return self._iframe_metrics