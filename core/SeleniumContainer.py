from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait
from selenium.common.exceptions import TimeoutException
import time
from selenium.webdriver.remote import webdriver
from selenium.webdriver.remote import webelement
from selenium.webdriver.support.event_firing_webdriver import EventFiringWebDriver



class SeleniumContainerException(Exception):
    pass


class SeleniumFacade:
    def __init__(self, driver):
        self.driver = driver

    def _find_deeper_element(self, parent_element, single_selector):
        try:
            result = self.is_xpath(single_selector)
            if result:
                return parent_element.find_element_by_xpath(single_selector)
            else:
                return parent_element.find_element_by_css_selector(single_selector)
        except Exception as ex:
            raise SeleniumContainerException("Timeout waiting for element", single_selector)

    def get_elements_count(self, selector):
        try:
            result = self.is_xpath(selector)
            if result:
                return self.driver.find_elements_by_xpath(selector)
            else:
                return self.driver.find_elements_by_css_selector(selector)
        except Exception as ex:
            raise SeleniumContainerException("Timeout waiting for elements", selector)

    def clear_input(self, selector):
        """
        Очистить поле ввода
        """
        element = self.get_element(selector)
        element.clear()

    def click_and_set_to_input(self, selector, value):
        self.click_button(selector)
        self.set_to_input(selector, value)

    def click_button(self, selector):
        element = self.get_element(selector)
        if not (element.is_enabled()):
            print("Element is disabled: ", selector)
        element.click()

    def click_button_if_visible(self, selector):
        self.element_wait_to_be_visible(selector)
        element = self.get_element(selector)
        if not (element.is_enabled()):
            print("Element is disabled: ", selector)
        element.click()

    def delete_all_cookies(self):
        self.driver.delete_all_cookies()

    def drag_and_drop(self, selector_element, selector_target):
        """
        Перетащить элемент
        """
        action_chains = ActionChains(self.driver)
        action_chains.drag_and_drop(self.get_element(selector_element), self.get_element(selector_target)).perform()

    def drag_and_drop_by_offset(self, selector, xoffset, yoffset):
        action_chains = ActionChains(self.driver)
        action_chains.drag_and_drop_by_offset(self.get_element(selector), xoffset, yoffset).perform()

    def element_wait(self, selector, value=30):
        """
        Ожидание проверки наличия элемента в DOM страницы.
        Но это не гарантирует, что элемент виден.
        """
        try:
            wait = WebDriverWait(self.driver, value)
            result = self.is_xpath(selector)
            if result:
                wait.until(EC.presence_of_element_located((By.XPATH, selector)))
            else:
                wait.until(EC.presence_of_element_located((By.CSS_SELECTOR, selector)))
        except Exception as ex:
            raise SeleniumContainerException("Timeout waiting for element", selector)

    def element_wait_to_be_visible(self, selector, value=30):
        """
        Ожидать, пока элемента появится в DOM страницы и станет видимым.
        Видимость означает, что элемент не только отображается, но также имеет высоту и ширину, которые больше 0.
        """
        try:
            wait = WebDriverWait(self.driver, value)
            result = self.is_xpath(selector)
            if result:
                wait.until(EC.visibility_of_element_located((By.XPATH, selector)))
            else:
                wait.until(EC.visibility_of_element_located((By.CSS_SELECTOR, selector)))
        except Exception as ex:
            raise SeleniumContainerException("Timeout waiting for element", selector)

    def element_wait_to_be_clickable(self, selector, value=30):
        """
        Ожидает пока элемент станет видимым и вы сможете нажать на него
        """
        try:
            wait = WebDriverWait(self.driver, value)
            result = self.is_xpath(selector)
            if result:
                wait.until(EC.element_to_be_clickable((By.XPATH, selector)))
            else:
                wait.until(EC.element_to_be_clickable((By.CSS_SELECTOR, selector)))
        except Exception as ex:
            raise SeleniumContainerException("Timeout waiting for element or element is disabled", selector)

    def element_wait_until_not_visible(self, selector, value=0.5, attempts=60):
        """
        Ожидать пока элемент станет невидимым или исчезнит из DOM

        Wait until the element is either invisible or not present on the DOM
        :param selector: XPATH or CSS selector
        :param value: period of time to wait an element during the attempt
        :param attempts: shows how many times we'll check the element
        :return: True of False (visible or invisible)
        """
        searched = True
        for attempt in range(attempts):
            try:
                wait = WebDriverWait(self.driver, value)
                result = self.is_xpath(selector)
                if result:
                    wait.until(EC.invisibility_of_element_located((By.XPATH, selector)))
                    searched = True
                    break
                else:
                    wait.until(EC.invisibility_of_element_located((By.CSS_SELECTOR, selector)))
                    searched = True
                    break
            except TimeoutException as ex:
                searched = False
        return searched

    def element_wait_frame_to_be_available_and_switch_to_it(self, selector, value=30, delay=2):
        """
        Ожидать пока фрейм станет доступным и переключитья в него
        :param delay: добавил принудительное ожидание, в секундах
        """
        try:
            wait = WebDriverWait(self.driver, value)
            result = self.is_xpath(selector)
            if result:
                wait.until(EC.frame_to_be_available_and_switch_to_it((By.XPATH, selector)))
            else:
                wait.until(EC.frame_to_be_available_and_switch_to_it((By.CSS_SELECTOR, selector)))
            self.explicit_wait(delay)
        except Exception as ex:
            raise SeleniumContainerException("The frame is not available", selector)

    def element_wait_until_not(self, selector, value=30):
        try:
            wait = WebDriverWait(self.driver, value)
            self.set_implicit_wait(2)
            result = self.is_xpath(selector)
            if result:
                wait.until(EC.invisibility_of_element_located((By.XPATH, selector)))
                self.set_implicit_wait(value)
                return True
            else:
                wait.until(EC.invisibility_of_element_located((By.CSS_SELECTOR, selector)))
                self.set_implicit_wait(value)
        except Exception as ex:
            self.set_implicit_wait(value)
            return False

    def explicit_wait(self, value):
        time.sleep(value)

    def get_button_state(self, selector):
        element = self.get_element(selector)
        return element.is_enabled()

    def get_checkbox_state(self, selector):
        element = self.get_element(selector)
        return element.is_selected()

    def get_element(self, selector):
        return self._find_deeper_element(self.driver, selector)

    # def get_elements(self, selector):
    #     return self._find_deeper_elements(self.driver, selector)

    def get_name(self, selector):
        element = self.get_element(selector)
        return element.get_attribute("name")

    def get_page_by_url(self, url):
        self.driver.get(url)

    def get_placehldr(self, selector):
        element = self.get_element(selector)
        return element.get_attribute("placeholder")

    # def get_text(self, selector):
    #     element = self.get_element(selector)
    #     return element.text
    #
    # def get_text_strip(self, selector):
    #     element = self.get_element(selector)
    #     return element.text.strip()

    def get_value_old(self, selector):
        element = self.get_element(selector)
        return element.get_attribute("value")

    def is_element_present(self, selector):
        try:
            result = self.is_xpath(selector)
            if result:
                return self.driver.find_element_by_xpath(selector).is_displayed()
            else:
                return self.driver.find_element_by_css_selector(selector).is_displayed()
        except Exception as ex:
            return False

    def is_xpath(self, selector):
        if (selector[0] == '/') or (selector[0] == '*'):
            return True

    def move_to_element(self, selector):                 # аналог команды hover - навдение мышкой на центр элемента
        action_chains = ActionChains(self.driver)
        action_chains.move_to_element(self.get_element(selector)).perform()


    def double_click(self, selector):  # двойной щелчек на элемент
        action_chains = ActionChains(self.driver)
        action_chains.double_click(self.get_element(selector)).perform()

    def context_click(self, selector):                   # щелчек на элемент правой кнопкой
        action_chains = ActionChains(self.driver)
        action_chains.context_click(self.get_element(selector)).perform()

    def refresh_page(self):
        self.driver.refresh()

    def set_checkbox(self, selector, value):
        element = self.get_element(selector)
        if not (element.is_enabled()):
            print("Element is disabled: ", selector)
        if element.is_selected() and not value:
            element.click()
        elif not (element.is_selected()) and value:
            element.click()

    def set_implicit_wait(self, value):
        self.driver.implicitly_wait(value)

    def set_to_input(self, selector, value):
        element = self.get_element(selector)
        element.send_keys(value)

    def stop_driver(self):
        self.driver.close()

    def switch_to_default(self):
        self.driver.switch_to.default_content()

    def switch_to_iframe(self, selector):
        element = self.get_element(selector)
        self.driver.switch_to.frame(element)

    def to_relative_url(self, url, relative_url):
        self.driver.get(url + relative_url)

    def switch_to_window(self, window_name):
        """
        Преключиться в указанное окно браузера
        example -> driver.switch_to.window(tabs1[2])
        """
        self.driver.switch_to.window(window_name)


    def get_text(self, selector, value=0.5, attempts=120):    # увеличил время вычитывания текста
        """
        Получить текст по селектору
        :param selector: XPATH or CSS selector
        :param value: period of time to wait an element during the attempt
        :param attempts: shows how many times we'll check the element
        :return: text of the element, stripped with spaces
        """
        for attempt in range(attempts):
            element = self.get_element(selector)
            if element.text.strip() == '':
                self.explicit_wait(value)
                continue
            else:
                return element.text.strip()
        return ""

    def get_text_strip(self, selector, value=0.5, attempts=120):    # увеличил время вычитывания текста
        """
        _strip - обрезает пробелы искомого текста с двух сторон
        :param selector: XPATH or CSS selector
        :param value: period of time to wait an element during the attempt
        :param attempts: shows how many times we'll check the element
        :return: text of the element, stripped with spaces
        """
        for attempt in range(attempts):
            element = self.get_element(selector)
            if element.text.strip() == '':
                self.explicit_wait(value)
                continue
            else:
                return element.text.strip()
        return ""

    def get_value(self, selector, value=0.5, attempts=120):   # увеличил время получения значения
        """
        Получить значение из файла (например из файла локализации)
        :param selector: XPATH or CSS selector
        :param value: period of time to wait an element during the attempt
        :param attempts: shows how many times we'll check the element
        :return: value of the element
        """
        for attempt in range(attempts):
            element = self.get_element(selector)
            if element.get_attribute("value").strip() == '':
                self.explicit_wait(value)
                continue
            else:
                return element.get_attribute("value").strip()
        return ""


    def send_keys_symbols(self, selector, value):       # метод для эмуляции нажатия нескольких клавиш
        element = self.get_element(selector)
        if value == 'Ctrl+a':
            element.send_keys(Keys.CONTROL + 'a')
        elif value == 'Ctrl+c':
            element.send_keys(Keys.CONTROL + 'c')
        elif value == 'Ctrl+s':
            element.send_keys(Keys.CONTROL + 's')
        elif value == 'Up':
            element.send_keys(Keys.UP)
        elif value == 'Down':
            element.send_keys(Keys.DOWN)
        elif value == 'Left':
            element.send_keys(Keys.LEFT)
        elif value == 'Right':
            element.send_keys(Keys.RIGHT)
        elif value == 'Enter':
            element.send_keys(Keys.ENTER)
        elif value == 'Backspace':
            element.send_keys(Keys.BACKSPACE)
        elif value == 'Go to next open tab':
            element.send_keys(Keys.CONTROL + Keys.TAB)
        elif value == 'Go to previous open tab':
            element.send_keys(Keys.CONTROL + Keys.SHIFT + Keys.TAB)
        elif value == 'Go to 1 tab':
            element.send_keys(Keys.CONTROL + Keys.NUMPAD1)
        elif value == 'Go to 2 tab':
            element.send_keys(Keys.CONTROL + Keys.NUMPAD2)
        elif value == 'Go to 3 tab':
            element.send_keys(Keys.CONTROL + Keys.NUMPAD3)



'''        т.е. я говорю сколько раз буду пытаться и какая задержка между попытками
        если последняя попытка вернула "" - пусто - то я возвращаю пусто

        часто бывает: assert selenium_facade.get_text() == localization.get_value

        и страница походу еще не прогрузилась и в атрибуте еще пусто - ну и селениум уже считал пусто и говорит - не равно!
        а на самом деле - подожди мля! вот я и заставляю подождать
        используется для проверки текста из инпутов, тайтлов, чего угодно

'''

