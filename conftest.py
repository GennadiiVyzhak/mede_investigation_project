from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from core.ConfigurationLoader import ConfigurationLoader
from core.LocalizationProvider import LocalizationProvider
from core.ScreenshotMaker import ScreenshotMaker
from core.SeleniumContainer import SeleniumFacade
from pages.LoginPage import LoginPage
import allure
import pytest

"""
Метод/фикстура configuration, принимает параметром config
если в настройках pytest config = "stage", тогда будет использоваться файл configuration_stage.txt
иначе - файл configuration_demo.txt
"""
@pytest.fixture(scope="function")
def configuration(config):
    if config == "stage":
        return ConfigurationLoader("configuration_stage.txt")
    elif config == "demo":
        return ConfigurationLoader("configuration_demo.txt")

"""
1. Метод/фикстура driver, принимает параметрами: configuration, localization, browser
если в настройках pytest browser == "chrome", тогда будет использоваться браузер chrome
иначе - файл браузер firefox
2.1 driver.maximize_window() - открыть окно браузера на весь экран
2.2 выполняем команды которые используют значения из файла configuration_stage/demo.txt
2.3 configuration_stage.txt
2.4 login_page = LoginPage() - создаем объект страницы
2.5 выполняем команды которые используют значения из файла configuration_stage/demo.txt
2.6 выполняем команды которые используют значения из файла title_resource_EN.txt
3. Ключевого слова yield, указывает стонужно сделать после окончания теста
3.1 driver.quit() - закрываем браузер
"""
@pytest.yield_fixture(scope="function")
def driver(configuration, localization, browser):
    chrome_options = Options()
    chrome_options.add_argument('--no-sandbox')
    if browser == "chrome":
        driver = webdriver.Chrome(options=chrome_options)
    elif browser == "firefox":
        driver = webdriver.geckodriver()

    driver.maximize_window()
    driver.implicitly_wait(configuration.get_value("implicit_wait"))
    driver.get(configuration.get_value("url"))
    login_page = LoginPage()
    driver.find_element_by_xpath(login_page.get_input_username()).send_keys(configuration.get_value("username"))
    driver.find_element_by_css_selector(login_page.get_input_password()).send_keys(configuration.get_value("password"))
    driver.find_element_by_css_selector(login_page.get_btn_login()).click()
    """ формируем окончательный url, по которому переходим на сайт: https://uk-stage.medeanalytics.co.uk/StartPage """
    driver.get(configuration.get_value("url") + localization.get_value("links", "start_page"))
    yield driver
    driver.quit()


@pytest.fixture(scope="function")
def localization(configuration):
    return LocalizationProvider(configuration.get_value("locale"))


""" Делаем настройки Pytest для запуска тестов """


def pytest_addoption(parser):
    parser.addoption("--config", action="append")
    parser.addoption("--browser", action="append")


def pytest_generate_tests(metafunc):
    if 'config' in metafunc.fixturenames:
        metafunc.parametrize("config", metafunc.config.option.config)
    if 'browser' in metafunc.fixturenames:
        metafunc.parametrize("browser", metafunc.config.option.browser)


@pytest.mark.tryfirst
def pytest_runtest_makereport(item, call, __multicall__):
    rep = __multicall__.execute()
    setattr(item, "rep_" + rep.when, rep)
    return rep


@pytest.fixture(scope="function")
def screenshot_maker(driver):
    return ScreenshotMaker(driver)


@pytest.fixture(scope='function', autouse=True)
def screenshot_on_failure(request, screenshot_maker):
    def screenshot():
        if request.node.rep_setup.failed:
            allure.attach(
                name=request.node.name,
                body=screenshot_maker.capture_screenshot(),
                attachment_type=allure.attachment_type.PNG,
            )
        elif request.node.rep_setup.passed:
            if request.node.rep_call.failed:
                allure.attach(
                            name=request.node.name,
                            body=screenshot_maker.capture_screenshot(),
                            attachment_type=allure.attachment_type.PNG,
                        )
    request.addfinalizer(screenshot)

"""
Метод/фикстура selenium_facade возвращает объект driver"""


@pytest.fixture(scope="function")
def selenium_facade(driver):
    return SeleniumFacade(driver)
