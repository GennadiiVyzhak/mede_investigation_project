import allure
from pages.Home_Page.HomePage import HomePage
from pages.Summary_Reports.DailyAESummryReportPage import DailyAESummaryReport
from pages.Detail_Reports.DailyAEDetailReportPage import DailyAEDetailReport
from pages.Common_Page.CommonPage import CommonPage
import time


@allure.epic("Home")
class TestFlatDimensionVerification:                     # наименование класса должно начинаться с: Test

    @allure.title("Flat dimension verification")         # наименование отчета о тесте
    @allure.description("Flat dimension verification")   # писание отчета о тесте
    def test_flat_dimension(self, localization, selenium_facade): # наименование метода теста должно начинаться с: test_

        home_page = HomePage()
        summary = DailyAESummaryReport()        # создаем объекты сраниц
        detail = DailyAEDetailReport()
        common = CommonPage()


        with allure.step("Step 1. Examine the welcome title"):

            selenium_facade.element_wait_frame_to_be_available_and_switch_to_it(common.get_iframe_main())

            assert selenium_facade.get_text_strip(home_page.get_title_Welcome_Gennadii_Vyzhak()) == \
                   localization.get_value("titles", "Welcome_Gennadii_Vyzhak")

            selenium_facade.switch_to_default()

            selenium_facade.click_button(home_page.get_menu_item_data_sets())
            selenium_facade.move_to_element(home_page.get_menu_item_daily_provider_data_sets())
            selenium_facade.click_button(home_page.get_menu_item_daily_a_e_summary_report())

        with allure.step("Step 2. Examine metrics"):
            selenium_facade.element_wait_to_be_visible(common.get_iframe_main())  # ждем пока загрузится страница и будет доступен элемент нужного iframe
            selenium_facade.element_wait_frame_to_be_available_and_switch_to_it(common.get_iframe_main())  # переключаемся в нужный iframe
            # selenium_facade.explicit_wait(5)   # ожидаем пока полностью подгрузится страница и будет доступен элемент тайтла
            selenium_facade.element_wait_to_be_clickable(summary.get_iframe_metric_menu())
            selenium_facade.click_button(summary.get_iframe_metric_menu())
            selenium_facade.explicit_wait(2)
            selenium_facade.switch_to_default()  # возвращаемся к дефолтному фрейму
            selenium_facade.element_wait_frame_to_be_available_and_switch_to_it(summary.get_iframe_metrics())   # переключаемся в нужный iframe

            assert selenium_facade.get_text(summary.get_title_select_metrics()) == \
                   localization.get_value("titles", "select_metrics")

            # selenium_facade.explicit_wait(5)
            selenium_facade.element_wait_to_be_clickable(summary.get_iframe_metric_No_of_Attendances())
            selenium_facade.double_click(summary.get_iframe_metric_No_of_Attendances())
            selenium_facade.double_click(summary.get_iframe_metric_No_of_Patients())
            selenium_facade.click_button(summary.get_iframe_metric_button_Apply())
            # selenium_facade.context_click(summary.get_iframe_metric_No_of_Attendances())

            selenium_facade.switch_to_default()
            selenium_facade.element_wait_frame_to_be_available_and_switch_to_it(common.get_iframe_main())
            selenium_facade.click_button(summary.get_button_Update())
            selenium_facade.explicit_wait(5)
            selenium_facade.element_wait_to_be_clickable(summary.get_metric_No_of_Attendances_value_1())
            selenium_facade.move_to_element(summary.get_metric_No_of_Attendances_value_1())
            selenium_facade.click_button(summary.get_metric_No_of_Attendances_value_1()) # кликнуть на значение метрики 1371

        with allure.step("Step 3. Detail report"):
            selenium_facade.switch_to_default()
            selenium_facade.element_wait_frame_to_be_available_and_switch_to_it(common.get_iframe_main())

            # selenium_facade.explicit_wait(5)
            assert selenium_facade.get_text(detail.get_title_Daily_A_E_Detail_Report()) == \
                localization.get_value("titles", "Daily_A_E_Detail_Report")

            time.sleep(2)




