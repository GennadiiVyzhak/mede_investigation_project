import allure
from pages.HomePage import HomePage
from pages.Summary_Reports.DailyAESummryReportPage import DailyAESummaryReport
from pages.Detail_Reports.DailyAEDetailReportPage import DailyAEDetailReport
import time


@allure.epic("Home")
class TestFlatDimensionVerification:                     # наименование класса должно начинаться с: Test

    @allure.title("Flat dimension verification")         # наименование отчета о тесте
    @allure.description("Flat dimension verification")   # писание отчета о тесте
    def test_flat_dimension(self, localization, selenium_facade): # наименование метода теста должно начинаться с: test_

        home_page = HomePage()
        summary = DailyAESummaryReport()        # создаем объекты сраниц
        detail = DailyAEDetailReport()



        with allure.step("Step 1. Click 3 menus on Home page"):     # прописываем шаги тестов
            selenium_facade.click_button(home_page.get_menu_item_data_sets())
            # selenium_facade.element_wait_to_be_visible(home_page.get_menu_item_daily_provider_data_sets())
            selenium_facade.move_to_element(home_page.get_menu_item_daily_provider_data_sets())
            selenium_facade.click_button(home_page.get_menu_item_daily_a_e_summary_report())


        with allure.step("Step 2. Examine the page title"):
            selenium_facade.element_wait_to_be_visible(summary.get_iframe_main())  # ждем пока загрузится страница и будет доступен элемент нужного iframe
            selenium_facade.element_wait_frame_to_be_available_and_switch_to_it(summary.get_iframe_main())  # переключаемся в нужный iframe
            selenium_facade.explicit_wait(5)   # ожидаем пока полностью подгрузится страница и будет доступен элемент тайтла
            # selenium_facade.element_wait_to_be_visible(summary.get_title_daily_a_e_summary_report())
            assert selenium_facade.get_text(summary.get_title_daily_a_e_summary_report()) == localization.get_value("titles", "daily_ae_summary_report")

        with allure.step("Step 3. Examine the Age dimension"):
            selenium_facade.click_button(summary.get_dimension_check_box())

            assert len(selenium_facade.get_elements_count(summary.get_dimension_count())) == 15 # проверяем количество дименшинов в выпадающем сиске

            selenium_facade.click_button(summary.get_item_age())
            selenium_facade.click_button(summary.get_button_update())

        with allure.step("Step 3. Examine the Age dimension"):
            selenium_facade.explicit_wait(2)
            selenium_facade.element_wait_to_be_visible(summary.get_metric_No_of_Attendances_value_1())

            v1 = selenium_facade.get_text_strip(summary.get_metric_No_of_Attendances_value_1()) # записать в переменную значение дименшина Age=18 -> v1=1,371
            print(v1) # вывести в отчет значение переменной v1=1,371
            selenium_facade.click_button(summary.get_metric_No_of_Attendances_value_1())  # кликнуть на значение метрики 1371

        with allure.step("Step 5. Detail report"):
            selenium_facade.switch_to_default()
            selenium_facade.element_wait_frame_to_be_available_and_switch_to_it(summary.get_iframe_main())

            assert selenium_facade.get_text(detail.get_title_Daily_A_E_Detail_Report()) == \
                localization.get_value("titles", "Daily_A_E_Detail_Report")

            selenium_facade.move_to_element(detail.get_Age_field())
            selenium_facade.click_button(detail.get_Age_field_checkbox())
            selenium_facade.click_button(detail.get_Age_Group_button())

            # selenium_facade.explicit_wait(10)
            selenium_facade.switch_to_default()
            # selenium_facade.explicit_wait(10)
            # selenium_facade.element_wait_to_be_visible(detail.get_iframe_field())
            selenium_facade.element_wait_frame_to_be_available_and_switch_to_it(detail.get_iframe_field())
            # selenium_facade.explicit_wait(5)
            selenium_facade.click_button(detail.get_iframe_field_Update_button())
            # selenium_facade.explicit_wait(5)

            selenium_facade.switch_to_default()
            selenium_facade.element_wait_frame_to_be_available_and_switch_to_it(summary.get_iframe_main())
            # selenium_facade.explicit_wait(5)

            selenium_facade.element_wait_to_be_visible(detail.get_field_Age_Query_totals_value())
            v2 = selenium_facade.get_text_strip(detail.get_field_Age_Query_totals_value())
            print(v2)

        with allure.step("Step 6. Value verification - Summary Report VS Detail Report"):

            assert v1 in v2         # содержит ли переменная v1=1,371, переменную v2=Query totals: 1,371
            assert v1 == v2[14:]    # равна ли переменная v1=1,371, модифицированной переменной v2=1,371

            time.sleep(2)





        # with allure.step("Step 4. Examine metrics"):
        #     selenium_facade.click_button(locator.get_metrics_menu())
        #     selenium_facade.explicit_wait(2)
        #     selenium_facade.switch_to_default()  # возвращаемся к дефолтному фрейму
        #     selenium_facade.element_wait_frame_to_be_available_and_switch_to_it(locator.get_iframe_metrics())   # переключаемся в нужный iframe
        #
        #     assert selenium_facade.get_text(locator.get_title_select_metrics()) == \
        #            localization.get_value("titles", "select_metrics")
        #
        #     selenium_facade.explicit_wait(5)
        #
        #     # selenium_facade.element_wait(locator.get_metric_No_of_Attendances())
        #     selenium_facade.double_click(locator.get_metric_No_of_Attendances())
        #     selenium_facade.double_click(locator.get_metric_No_of_Patients())
        #     selenium_facade.click_button(locator.get_metric_button_Apply())
        #     # selenium_facade.context_click(locator.get_metric_No_of_Attendances())
        #
        #     selenium_facade.switch_to_default()
        #     selenium_facade.element_wait_frame_to_be_available_and_switch_to_it(locator.get_iframe_main())
        #     selenium_facade.click_button(locator.get_button_update())
        #     selenium_facade.explicit_wait(5)
        #     selenium_facade.element_wait_to_be_visible(locator.get_metric_No_of_Attendances_value_1())
        #     selenium_facade.click_button(locator.get_metric_No_of_Attendances_value_1()) # кликнуть на значение метрики 1371
        #
        # with allure.step("Step 5. Detail report"):
        #     selenium_facade.switch_to_default()
        #     selenium_facade.element_wait_frame_to_be_available_and_switch_to_it(locator.get_iframe_main())
        #
        #     # selenium_facade.explicit_wait(5)
        #     assert selenium_facade.get_text(locator.get_title_Daily_A_E_Detail_Report()) == \
        #         localization.get_value("titles", "Daily_A_E_Detail_Report")

            # time.sleep(2)




