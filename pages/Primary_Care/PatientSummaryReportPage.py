
class PatientSummaryReportPage:

    _menu_data_sets_managament = "//a[contains(text(),'Data Sets Management')]"
    _menu_daily_provider_data_sets = "//li[@class='dropdown-submenu']//a[contains(text(),'DAILY PROVIDER DATA SETS')]"
    _menu_primary_care = "//html//body//div//div//div//div//div//div//div//ul//li//ul//li//a[contains(text(),'PRIMARY CARE')]"
    _menu_patient_summary_report = "//html//body//div//div//div//div//div//div//div//ul//li//ul//li//ul//li//a[contains(text(),'Patient Summary Report')]"


    def get_menu_data_sets_managament(self):
        return self._menu_data_sets_managament

    def get_menu_daily_provider_data_sets(self):
        return self._menu_daily_provider_data_sets

    def get_menu_primary_care(self):
        return self._menu_primary_care

    def get_menu_item_data_sets(self):
        return self._menu_patient_summary_report