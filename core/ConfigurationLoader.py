import json
import sys


class ConfigurationLoader:
    def __init__(self, filename):
        try:
            f = open(filename)
            json_data = f.read()
            self.config = json.loads(json_data)
            print("Configuration loaded from " + filename)   # при запуске теста выводим текст в Console
        except IOError as ex:
            print("Error in loading configuration", ex)
            sys.exit()

    def get_value(self, key):
        return self.config.get(key)

