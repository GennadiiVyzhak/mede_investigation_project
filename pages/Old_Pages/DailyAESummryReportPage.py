
class DailyAESummaryReportPage:

# Titles locators:
    _title_daily_a_e_summary_report = "//h3[contains(text(),'Daily A&E Summary Report')]"
    _title_Daily_A_E_Detail_Report = "//a[contains(text(),'Daily A&E Detail Report')]"
    _title_select_metrics = "//span[contains(text(),'Select Metrics')]"

# Button locators:
    _button_update = "//div[@id='container-dimpanel']//div//a[contains(text(),'Update')]"

# Frame locators:
    _iframe_main = "#frmMain"
    _iframe_metrics = "//iframe[@id='dialogFrame0']"

# Dimension locators:
    _dimension_check_box = "//div[@id='s2id_autogen1']//a//span//b"
    _dimension_field = "//input[@id='s2id_autogen2_search']"
    _item_age = "//div[text()='Age']"
    _age_value = "//tr[@id='tr_TC0_0_0']//td[2]//span[1]"

# Metric frame locators:
    _metrics_menu = "//a[@class='btn']"
    _metric_button_Apply = "//span[contains(text(),'Apply')]"
    _metric_No_of_Attendances = "//a[contains(text(),'% No. of Attendances')]"
    _metric_No_of_Patients = "//a[contains(text(),'% No. of Patients')]"

# Metric No of Patients locators:
    _metric_No_of_Patients_value_1 = "//tr[@id='tr_TC0_0_0']//td[3]//span[1]"  # 1014

# Metric No of Attendances locators:
#     _metric_No_of_Attendances_value_1 = "//tr[@id='tr_TC0_0_0']//td[2]//span[1]"  # 1371
    _metric_No_of_Attendances_value_1 = "/html[1]/body[1]/div[1]/div[1]/div[1]/div[4]/div[2]/div[2]/div[1]/div[2]/div[2]/table[1]/tbody[1]/tr[1]/td[2]/span[1]"

# Metric locators:
    _metric1_No_of_Patients = "//body//div[@id='reportTableContainer_TC0']//div//div[1]//table[1]//thead[1]//tr[1]//td[3]//a[1]"
    _metric1_No_of_Attendances = "//body//div[@id='reportTableContainer_TC0']//div//div[1]//table[1]//thead[1]//tr[1]//td[2]//a[1]"

# Other locators:
    _spinner = "span.icon.icon-circle-o-notch.icon-xx-large.icon-spin::before"
    _dimension_count = "//*[@class='select2-result-label']"

# Detail Reports locators:
    _Age_field_checkbox = "//table[@id='tblMain']//th[4]//div[1]//span[3]"
    _Age_field = "//th[@class='green']//span[contains(text(),'Age')]"
    _Age_Group_button = "//ul[@class='dropdown-menu']//a[contains(text(),'Group')]"
    # _iframe_field = '//*[@id="GroupByViewContainer"]'
    # _iframe_field = "/html/body/div[1]/div[5]/div[2]/div[2]/iframe"
    _iframe_field = "//iframe[contains(@id, 'dialogFrame')]"
    _iframe_field_Update_button = "//button[contains(text(),'Update')]"
    _field_Age_Query_totals_value = "//tr[@id='queryRow']//span[1]//span[1]"



# Titles methods:
    def get_title_daily_a_e_summary_report(self):
        return self._title_daily_a_e_summary_report

    def get_title_Daily_A_E_Detail_Report(self):
        return self._title_Daily_A_E_Detail_Report

    def get_title_select_metrics(self):
        return self._title_select_metrics

# Button locators:
    def get_button_update(self):
        return self._button_update

# Frame locators:
    def get_iframe_main(self):
        return self._iframe_main

    def get_iframe_metrics(self):
        return self._iframe_metrics

# Dimension locators:
    def get_dimension_check_box(self):
        return self._dimension_check_box

    def get_dimension_field(self):
        return self._dimension_field

    def get_item_age(self):
        return self._item_age

    def get_age_value(self):
        return self._age_value

# Metric locators:
    def get_metrics_menu(self):
        return self._metrics_menu

    def get_metric_button_Apply(self):
        return self._metric_button_Apply

    def get_metric_No_of_Attendances(self):
        return self._metric_No_of_Attendances

    def get_metric_No_of_Patients(self):
        return self._metric_No_of_Patients

# Metric No of Patients locators:
    def get_metric_No_of_Patients_value_1(self):
        return self._metric_No_of_Patients_value_1

# Metric No of Attendances locators:
    def get_metric_No_of_Attendances_value_1(self):
        return self._metric_No_of_Attendances_value_1

# Metric locators:
    def get_metric1_No_of_Patients(self):
        return self._metric1_No_of_Patients

    def get_metric1_No_of_Attendances(self):
        return self._metric1_No_of_Attendances

# Other locators:
    def get_spinner(self):
        return self._spinner

    def get_dimension_count(self):
        return self._dimension_count

# Detail Reports locators:
    def get_Age_field_checkbox(self):
        return self._Age_field_checkbox

    def get_Age_field(self):
        return self._Age_field

    def get_Age_Group_button(self):
        return self._Age_Group_button

    def get_iframe_field(self):
        return self._iframe_field

    def get_iframe_field_Update_button(self):
        return self._iframe_field_Update_button

    def get_field_Age_Query_totals_value(self):
        return self._field_Age_Query_totals_value