import allure
from pages.Home_Page.HomePage import HomePage
from pages.Summary_Reports.DailyAESummryReportPage import DailyAESummaryReport
from pages.Task_Manager_Tasks.TaskManagerTasksPage import TaskManagerTasks
from pages.Common_Page.CommonPage import CommonPage
import time


@allure.epic("Home")
class TestTaskManagerTasksVerification:                              # наименование класса должно начинаться с: Test

    @allure.title("Task Manager Tasks functionality verification")                  # наименование allure отчета
    @allure.description("Task Manager Tasks functionality verification")            # description allure отчета
    def test_task_manager_tasks(self, localization, selenium_facade): # наименование метода теста должно начинаться с: test_

        home_page = HomePage()
        summary = DailyAESummaryReport()        # создаем объекты сраниц
        task_manager = TaskManagerTasks()
        common = CommonPage()


        with allure.step("Step 1. Examine Welcome title"):

            selenium_facade.click_button(task_manager.get_icon_Tools())
            selenium_facade.move_to_element(task_manager.get_menu_item_Tasks_Services())
            selenium_facade.click_button(task_manager.get_menu_item_TaskManager_Tasks())

            # selenium_facade.send_keys_symbols(common.get_iframe_main(), 'Go to next open tab')
            selenium_facade.send_keys_symbols(common.get_iframe_main(), 'Go to 2 tab')

            time.sleep(2)

            # selenium_facade.switch_to_window('0')



        #     selenium_facade.click_button(home_page.get_menu_item_data_sets())
        #     selenium_facade.move_to_element(home_page.get_menu_item_daily_provider_data_sets())
        #     selenium_facade.click_button(home_page.get_menu_item_daily_a_e_summary_report())
        #
        # with allure.step("Step 2. Examine Summary Report title"):
        #     selenium_facade.element_wait_to_be_visible(common.get_iframe_main())
        #     selenium_facade.element_wait_frame_to_be_available_and_switch_to_it(common.get_iframe_main())  # переключаемся в общий iframe
        #     selenium_facade.explicit_wait(3)   # ожидаем пока полностью подгрузится страница и будет доступен элемент тайтла
        #
        #     # selenium_facade.element_wait_to_be_visible(summary.get_title_daily_a_e_summary_report())
        #     selenium_facade.move_to_element(summary.get_title_daily_a_e_summary_report())
        #     assert selenium_facade.get_text_strip(summary.get_title_daily_a_e_summary_report()) == \
        #            localization.get_value("titles", "daily_ae_summary_report")
        #
        # with allure.step("Step 3. Examine number of dimensions"):
        #     selenium_facade.element_wait_to_be_clickable(summary.get_dimension_check_box())
        #     selenium_facade.click_button(summary.get_dimension_check_box())
        #
        #     assert len(selenium_facade.get_elements_count(summary.get_dimension_count())) == 15 # проверяем количество дименшинов в выпадающем сиске
        #
        # with allure.step("Step 4. Examine Age dimension"):
        #     selenium_facade.click_button(summary.get_dimension_Age())
        #     selenium_facade.click_button(summary.get_button_Update())
        #
        #
        #     selenium_facade.explicit_wait(2)
        #     selenium_facade.element_wait_to_be_visible(summary.get_dimension_Age_value_1())
        #     selenium_facade.move_to_element(summary.get_dimension_Age_value_1())
        #     # selenium_facade.element_wait_to_be_clickable(summary.get_dimension_Age_value_1())
        #
        #
        #     # selenium_facade.explicit_wait(2)
        #     # selenium_facade.element_wait_to_be_visible(summary.get_metric_No_of_Attendances_value_1())
        #
        #     v1 = selenium_facade.get_text_strip(summary.get_dimension_Age_value_1()) # записать в переменную значение дименшина Age=18 -> v1=1,371
        #     print(v1) # вывести в отчет значение переменной v1=1,371
        #     selenium_facade.click_button(summary.get_dimension_Age_value_1())  # кликнуть на значение дименшина 1371
        #
        # with allure.step("Step 5. Examine Summary Detail title"):
        #     selenium_facade.switch_to_default()
        #     selenium_facade.element_wait_frame_to_be_available_and_switch_to_it(common.get_iframe_main())
        #
        #     assert selenium_facade.get_text(detail.get_title_Daily_A_E_Detail_Report()) == \
        #         localization.get_value("titles", "Daily_A_E_Detail_Report")
        #
        #     selenium_facade.move_to_element(detail.get_Age_field())
        #     selenium_facade.click_button(detail.get_Age_field_checkbox())
        #     selenium_facade.click_button(detail.get_Group_button())
        #
        #     selenium_facade.switch_to_default()
        #     selenium_facade.element_wait_frame_to_be_available_and_switch_to_it(detail.get_iframe_field())
        #     selenium_facade.click_button(detail.get_iframe_field_Update_button())
        #
        #     selenium_facade.switch_to_default()
        #     selenium_facade.element_wait_frame_to_be_available_and_switch_to_it(common.get_iframe_main())
        #
        #     selenium_facade.element_wait_to_be_visible(detail.get_field_Age_Query_totals_value())
        #     v2 = selenium_facade.get_text_strip(detail.get_field_Age_Query_totals_value())
        #     print(v2)
        #
        # with allure.step("Step 6. Value verification = Summary Report VS Detail Report"):
        #
        #     assert v1 in v2         # содержит ли переменная v1=1,371, переменную v2=Query totals: 1,371
        #     assert v1 == v2[14:]    # равна ли переменная v1=1,371, модифицированной переменной v2=1,371
        #
        #     time.sleep(2)





