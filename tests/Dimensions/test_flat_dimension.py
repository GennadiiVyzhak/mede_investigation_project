import allure
from pages.Home_Page.HomePage import HomePage
from pages.Summary_Reports.DailyAESummryReportPage import DailyAESummaryReport
from pages.Detail_Reports.DailyAEDetailReportPage import DailyAEDetailReport
from pages.Common_Page.CommonPage import CommonPage
import time


@allure.epic("Home")
class TestFlatDimensionVerification:                              # наименование класса должно начинаться с: Test

    @allure.title("Flat dimension verification")                  # наименование allure отчета
    @allure.description("Flat dimension verification")            # description allure отчета
    def test_flat_dimension(self, localization, selenium_facade): # наименование метода теста должно начинаться с: test_

        home_page = HomePage()
        summary = DailyAESummaryReport()        # создаем объекты сраниц
        detail = DailyAEDetailReport()
        common = CommonPage()


        with allure.step("Step 1. Examine Welcome title"):

            selenium_facade.element_wait_frame_to_be_available_and_switch_to_it(common.get_iframe_main())

            assert selenium_facade.get_text_strip(home_page.get_title_Welcome_Gennadii_Vyzhak()) == \
                   localization.get_value("titles", "Welcome_Gennadii_Vyzhak")

            selenium_facade.switch_to_default()

            selenium_facade.click_button(home_page.get_menu_item_data_sets())
            selenium_facade.move_to_element(home_page.get_menu_item_daily_provider_data_sets())
            selenium_facade.click_button(home_page.get_menu_item_daily_a_e_summary_report())

        with allure.step("Step 2. Examine Summary Report title"):
            selenium_facade.element_wait_to_be_visible(common.get_iframe_main())
            selenium_facade.element_wait_frame_to_be_available_and_switch_to_it(common.get_iframe_main())  # переключаемся в общий iframe
            selenium_facade.explicit_wait(5)   # ожидаем пока полностью подгрузится страница и будет доступен элемент тайтла

            # selenium_facade.element_wait_to_be_visible(summary.get_title_daily_a_e_summary_report())
            selenium_facade.move_to_element(summary.get_title_daily_a_e_summary_report())
            assert selenium_facade.get_text_strip(summary.get_title_daily_a_e_summary_report()) == \
                   localization.get_value("titles", "daily_ae_summary_report")

        with allure.step("Step 3. Examine number of dimensions"):
            selenium_facade.element_wait_to_be_clickable(summary.get_dimension_check_box())
            selenium_facade.click_button(summary.get_dimension_check_box())

            assert len(selenium_facade.get_elements_count(summary.get_dimension_count())) == 15 # проверяем количество дименшинов в выпадающем сиске

        with allure.step("Step 4. Examine Age dimension"):
            selenium_facade.click_button(summary.get_dimension_Age())
            selenium_facade.click_button(summary.get_button_Update())


            selenium_facade.explicit_wait(2)
            selenium_facade.element_wait_to_be_visible(summary.get_dimension_Age_value_1())
            selenium_facade.move_to_element(summary.get_dimension_Age_value_1())
            # selenium_facade.element_wait_to_be_clickable(summary.get_dimension_Age_value_1())


            # selenium_facade.explicit_wait(2)
            # selenium_facade.element_wait_to_be_visible(summary.get_metric_No_of_Attendances_value_1())

            v1 = selenium_facade.get_text_strip(summary.get_dimension_Age_value_1()) # записать в переменную значение дименшина Age=18 -> v1=1,371
            print('v1 = '+v1) # вывести в отчет значение переменной v1=1,371
            selenium_facade.click_button(summary.get_dimension_Age_value_1())  # кликнуть на значение дименшина 1371

        with allure.step("Step 5. Examine Summary Detail title"):
            selenium_facade.switch_to_default()
            selenium_facade.element_wait_frame_to_be_available_and_switch_to_it(common.get_iframe_main())

            assert selenium_facade.get_text(detail.get_title_Daily_A_E_Detail_Report()) == \
                localization.get_value("titles", "Daily_A_E_Detail_Report")

            selenium_facade.move_to_element(detail.get_Age_field())
            selenium_facade.click_button(detail.get_Age_field_checkbox())
            selenium_facade.click_button(detail.get_Group_button())

            selenium_facade.switch_to_default()
            selenium_facade.element_wait_frame_to_be_available_and_switch_to_it(detail.get_iframe_field())
            selenium_facade.click_button(detail.get_iframe_field_Update_button())

            selenium_facade.switch_to_default()
            selenium_facade.element_wait_frame_to_be_available_and_switch_to_it(common.get_iframe_main())

            selenium_facade.explicit_wait(7)
            selenium_facade.explicit_wait(7)
            selenium_facade.explicit_wait(7)


            selenium_facade.element_wait_to_be_visible(detail.get_field_Age_Query_totals_value())
            v2 = selenium_facade.get_text_strip(detail.get_field_Age_Query_totals_value())
            print('v2 = '+v2)


        with allure.step("Step 6. Value verification: Summary Report VS Detail Report"):

            assert v1 in v2         # содержит ли переменная v1=1,371, переменную v2=Query totals: 1,371
            assert v1 == v2[14:]    # равна ли переменная v1=1,371, модифицированной переменной v2=1,371

            time.sleep(2)





        # with allure.step("Step 4. Examine metrics"):
        #     selenium_facade.click_button(summary.get_iframe_metric_menu())
        #     selenium_facade.explicit_wait(2)
        #     selenium_facade.switch_to_default()  # возвращаемся к дефолтному фрейму
        #     selenium_facade.element_wait_frame_to_be_available_and_switch_to_it(summary.get_iframe_metrics())   # переключаемся в нужный iframe
        #
        #     assert selenium_facade.get_text(summary.get_title_select_metrics()) == \
        #            localization.get_value("titles", "select_metrics")
        #
        #     selenium_facade.explicit_wait(5)
        #
        #     selenium_facade.double_click(summary.get_iframe_metric_No_of_Attendances())
        #     selenium_facade.double_click(summary.get_iframe_metric_No_of_Patients())
        #     selenium_facade.click_button(summary.get_iframe_metric_button_Apply())
        #     # selenium_facade.context_click(summary.get_iframe_metric_No_of_Attendances())
        #
        #     selenium_facade.switch_to_default()
        #     selenium_facade.element_wait_frame_to_be_available_and_switch_to_it(common.get_iframe_main())
        #     selenium_facade.click_button(summary.get_button_Update())
        #     selenium_facade.explicit_wait(5)
        #     selenium_facade.element_wait_to_be_visible(summary.get_metric_No_of_Attendances_value_1())
        #     selenium_facade.click_button(summary.get_metric_No_of_Attendances_value_1()) # кликнуть на значение метрики 1371
        #
        # with allure.step("Step 5. Detail report"):
        #     selenium_facade.switch_to_default()
        #     selenium_facade.element_wait_frame_to_be_available_and_switch_to_it(common.get_iframe_main())
        #
        #     # selenium_facade.explicit_wait(5)
        #     assert selenium_facade.get_text(detail.get_title_Daily_A_E_Detail_Report()) == \
        #         localization.get_value("titles", "Daily_A_E_Detail_Report")
        #
        #     time.sleep(2)




