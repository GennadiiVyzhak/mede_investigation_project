
class HomePage:


# Titles locators:
    _title_Welcome_Gennadii_Vyzhak = "//h3[contains(text(),'Welcome, Gennadii Vyzhak')]"
    _menu_item_data_sets = "//a[contains(text(),'Data Sets Management')]"
    _menu_item_daily_provider_data_sets = "//li[@class='dropdown-submenu']//a[contains(text(),'DAILY PROVIDER DATA SETS')]"
    _menu_item_daily_a_e_summary_report = "//ul[@class='dropdown-menu']/li/a[text()='Daily A&E Summary Report']"


    # Titles methods:
    def get_title_Welcome_Gennadii_Vyzhak(self):
        return self._title_Welcome_Gennadii_Vyzhak

    def get_menu_item_daily_a_e_summary_report(self):
        return self._menu_item_daily_a_e_summary_report

    def get_menu_item_daily_provider_data_sets(self):
        return self._menu_item_daily_provider_data_sets

    def get_menu_item_data_sets(self):
        return self._menu_item_data_sets

    def get_item_Collaborations(self):
        return self._item_Collaborations