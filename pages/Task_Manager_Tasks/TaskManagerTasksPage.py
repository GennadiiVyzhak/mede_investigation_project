
class TaskManagerTasks:


# Titles locators:
    _icon_Tools = "//a[@id='tool-icon-tools']//span"
    _menu_item_Tasks_Services = "//div[@id='main-menu']//div//div//div//a[contains(text(),'Tasks Services')]"
    _menu_item_TaskManager_Tasks = "//div[@id='main-menu']//div//div//div//a[contains(text(),'TaskManager Tasks')]"
    _ID_field = "//li[1]//div[1]//input[1]"


    # Titles methods:
    def get_icon_Tools(self):
        return self._icon_Tools

    def get_menu_item_Tasks_Services(self):
        return self._menu_item_Tasks_Services

    def get_menu_item_TaskManager_Tasks(self):
        return self._menu_item_TaskManager_Tasks

    def get_ID_field(self):
        return self._ID_field

    def get_item_Collaborations(self):
        return self._item_Collaborations