import allure
from pages.Old_Pages.DailyAESummryReportPage import DailyAESummaryReportPage
from pages.HomePage import HomePage
import time


@allure.epic("Home")
class TestActions:                                           # наименование класса должно начинаться с: Test

    @allure.title("Log in with a valid user")                # наименование отчета о тесте
    @allure.description("Log in with a valid user")          # писание отчета о тесте
    def test_actions(self, localization, selenium_facade):   # наименование метода теста должно начинаться с: test_

        locator = DailyAESummaryReportPage()                 # создаем объекты сраниц
        home_page = HomePage()


        with allure.step("Step 1. Click 3 menus on Home page"):     # прописываем шаги тестов
            selenium_facade.click_button(home_page.get_menu_item_data_sets())
            # selenium_facade.element_wait_to_be_visible(home_page.get_menu_item_daily_provider_data_sets())
            selenium_facade.move_to_element(home_page.get_menu_item_daily_provider_data_sets())
            selenium_facade.click_button(home_page.get_menu_item_daily_a_e_summary_report())


        with allure.step("Step 2. Examine the page title"):
            selenium_facade.element_wait_to_be_visible(locator.get_iframe_main())  # ждем пока загрузится страница и будет доступен элемент нужного iframe
            selenium_facade.element_wait_frame_to_be_available_and_switch_to_it(locator.get_iframe_main())  # переключаемся в нужный iframe
            selenium_facade.explicit_wait(8)   # ожидаем пока полностью подгрузится страница и будет доступен элемент тайтла
            # selenium_facade.element_wait(locator.get_title_daily_a_e_summary_report())
            selenium_facade.drag_and_drop(locator.get_metric1_No_of_Patients(), locator.get_metric1_No_of_Attendances())  # переянуть метрику

            time.sleep(10)


